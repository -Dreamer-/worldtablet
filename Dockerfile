FROM php:7.2-apache

RUN chmod -R 777 /var/www/html/

RUN docker-php-ext-install mysqli
RUN docker-php-ext-install mbstring

RUN apt-get update -y && apt-get install -y libfreetype6-dev libjpeg62-turbo-dev libpng-dev

RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/
RUN docker-php-ext-install -j$(nproc) gd

RUN a2enmod rewrite

RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf
RUN echo "127.0.0.1 localhost" >> /etc/hostname

RUN service apache2 restart

ENV TZ=Europe/Kiev

WORKDIR /var/www/html

