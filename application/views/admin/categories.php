<div id="main">
    <div class="full_w">
        <div class="h_title">Manage categories - table</div>

        <?php if(!empty($categories)): ?>
    <table>
        <thead>
        <tr>
            <th scope="col" style="width: 45px;">ID</th>
            <th scope="col">Title</th>
            <th scope="col" style="width: 55px;">Modify</th>
        </tr>
        </thead>

        <tbody>
        <?php foreach($categories as $key => $j): ?>
            <tr>
                <td class="align-center"><?php echo $j->categ_id;?></td>
                <td><?php echo $j->title;?></td>
                <td>
                    <a href="/admin/categ_edit/<?php echo $j->categ_id ?>" class="table-icon edit" title="Edit"></a>
                    <!--<a href="#" class="table-icon archive" title="Archive"></a>-->
                    <a href="/admin/categ_delete/<?php echo $j->categ_id; ?>" class="table-icon delete" title="Delete"></a>
                </td>
            </tr>
        <?php endforeach ?>
        <?php else: echo "<div class='n_warning'><p><b>no categories</b></p></div>";?>
        <?php endif;?>
        </tr>
        </tbody>
    </table>
        <div class="entry">
            <div class="sep"></div>
            <form method="post" action="">
                <label for="title">Add new category:</label>
                <input type="text" name="title" style="width: 200px">
                <button type="submit" class="ok">Ok</button>
            </form>
            <div class="sep"></div>
            <a class="button" href="/admin/goods">Goods</a>
        </div>
    </div>
</div>