<div class="container-fluid">
    <div class="side-body">
        <div class="page-title">
            <span class="title">Table</span>
            <div class="description">A bootstrap table for display list of data.</div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">

                        <div class="card-title">
                            <div class="title">Товары</div>
                        </div>
                    </div>
                    <?php if(!empty($goods)): ?>
                        <div class="card-body">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th><a href="/admin/goods">ID</a> <span class="icon icon-sort glyphicon glyphicon-sort"></span></th>
                                    <th><a href="/admin/goods/1">Title</a> <span class="icon icon-sort glyphicon glyphicon-sort"></span></th>
                                    <th><a href="/admin/goods/2">Gategory ID</a> <span class="icon icon-sort glyphicon glyphicon-sort"></span></th>
                                    <th><a href="/admin/goods/3">Gategory</a> <span class="icon icon-sort glyphicon glyphicon-sort"></span></th>
                                    <th><a href="/admin/goods/4">Price</a> <span class="icon icon-sort glyphicon glyphicon-sort"></span></th>
                                    <th>Image</th>
                                    <th>Modify</th>
                                </tr>
                        </thead>

                        <tbody>
                        <?php foreach($goods as $key => $j): ?>
                            <tr>
                                <td><?php echo $j->goods_id;?></td>
                                <td><?php echo $j->goods_title;?></td>
                                <td><?php echo $j->categ_id;?></td>
                                <td><?php echo $j->title;?></td>
                                <td>$<?php echo $j->price;?></td>
                                <td><img src="/upload/timthumb.php?src=goods/<?php echo $j->image; ?>&h=70" style="max-width:80px; border-radius: 5px"/></td>
                                <td>
                                    <a href="/admin/goods_edit/<?php echo $j->goods_id;?>" class="icon fa fa-edit" title="Edit"></a>
                                    <!--<a href="#" class="table-icon archive" title="Archive"></a>-->
                                    <a href="/admin/goods_delete/<?php echo $j->goods_id;?>" class="icon fa fa-trash" id="delete" title="Delete"></a>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    <?php else: echo "<div class='n_warning'><p><b>no categories</b></p></div>";?>
                    <?php endif;?>
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="entry">
            <div class="sep"></div>
            <a class="button add" href="/admin/new_goods">Add new goods</a>
        </div>
    </div>
</div>