<div id="main">
    <div class="full_w">
        <div class="h_title">Edit category - form elements</div>
        <?php if(!empty(validation_errors())): ?>
            <div class="n_error"><?php echo validation_errors(); ?></div>
        <?php endif; ?>
        <?php if(!empty($error_id)) {echo "<div class='n_error'>$error_id</div>";} ?>
        <form action="" method="post">
            <div class="element">
                <label for="title">Category ID <span class="red">(required)</span></label>
                <input id="title" name="id" class="text err" value="<?php echo $content->categ_id;?>"/>
            </div>
            <div class="element">
                <label for="name">Category title <span>(required)</span></label>
                <input id="link" name="title" class="text" value="<?php echo $content->title;?>"/>
            </div>
            <div class="entry">
                <button type="submit" class="ok">Save</button> <!--<button class="cancel" href="/admin/pages">Cancel</button>-->
            </div>
        </form>
    </div>
</div>