<!DOCTYPE html>
<html>

<head>
	<title><?php echo $page_title, ' - ', $page_name ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300,400' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
	<!-- CSS Libs -->
	<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="/css/animate.min.css">
	<link rel="stylesheet" type="text/css" href="/css/bootstrap-switch.min.css">
	<link rel="stylesheet" type="text/css" href="/css/checkbox3.min.css">
	<link rel="stylesheet" type="text/css" href="/css/jquery.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="/css/dataTables.bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/css/select2.min.css">
	<!-- CSS App -->
	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<link rel="stylesheet" type="text/css" href="/css/themes/flat-blue.css">
</head>

<body class="flat-blue login-page">
<div class="container">
	<div class="login-box">
		<div>
			<div class="login-form row">
				<div class="col-sm-12 text-center login-header">
					<i class="login-logo fa fa-connectdevelop fa-5x"></i>
					<h4 class="login-title">Flat Admin V2</h4>
				</div>
				<div class="col-sm-12">
					<div class="login-body">
						<div class="progress hidden" id="login-progress">
							<div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
								Log In...
							</div>
						</div>
						<form action="" method="post">
							<?php if(isset($error_message)): ?>
								<div class="alert fresh-color alert-danger"><?php echo $error_message; ?></div>
							<?php elseif(!empty(validation_errors())): ?>
								<div class="alert fresh-color alert-danger"><?php echo validation_errors(); ?></div>
							<?php endif; ?>
							<div class="control">
								<input id="login" class="form-control" name="username" type="text" placeholder="Username" />
							</div>
							<div class="control">
								<input id="pass" class="form-control" name="password" type="password" placeholder="Password" />
							</div>
							<?php if(!empty($captcha)):?>
								<label><?= $captcha;?></label>
								<input type="text" name="captcha" class="text" />
							<?php endif; ?>
							<div class="login-button text-center">
								<input type="submit" class="btn btn-primary" value="Login">
							</div>
						</form>
					</div>
					<div class="login-footer">
						<span class="text-right"><a href="#" class="color-white"><a href="/">WorldCat</a></a></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="/js/jquery.min.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/Chart.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-switch.min.js"></script>

<script type="text/javascript" src="/js/jquery.matchHeight-min.js"></script>
<script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="/js/select2.full.min.js"></script>
<script type="text/javascript" src="/js/ace/ace.js"></script>
<script type="text/javascript" src="/js/ace/mode-html.js"></script>
<script type="text/javascript" src="/js/ace/theme-github.js"></script>
<!-- Javascript -->
<script type="text/javascript" src="/js/app.js"></script>
</body>

</html>
