<div id="main">
    <div class="full_w">
        <div class="h_title">Edit account - form elements</div>
        <?php if(!empty(validation_errors())): ?>
            <div class="n_error"><?php echo validation_errors(); ?></div>
        <?php elseif(!empty($error_pass)): ?>
            <div class="n_error"><p><?php echo $error_pass; ?></p></div>
        <?php endif; ?>
        <form action="" method="post">
            <div class="element">
                <label for="title">Username</label>
                <input id="title" name="username" class="text" style="width: 200px" value="<?php echo $user->username ?>"/>
            </div>
            <div class="element">
                <label for="name">Password</label>
                <input type="password" name="password" style="width: 200px" value="<?php echo $user->password ?>"/>
            </div>
            <div class="element">
                <label for="name">Repeat password</label>
                <input type="password" name="repeat" style="width: 200px" value="<?php echo $user->password ?>"/>
            </div>
            <div class="entry">
                <button type="submit" class="ok">Save account</button> <!--<button class="cancel" href="/admin/pages">Cancel</button>-->
            </div>
        </form>
    </div>
</div>