<div id="main">
    <div class="full_w">
        <div class="h_title">Manage messages - table</div>

        <?php if(!empty($messages)): ?>
    <table>
        <thead>
        <tr>
            <th scope="col">№</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">text</th>
            <th scope="col" style="width: 70px;">Modify</th>
        </tr>
        </thead>

        <tbody>
        <?php $id = 1; ?>
            <?php foreach($messages as $key => $j): ?>
                <?php if($j->status === 'new'): ?>
                <?= '<tr style="background-color: #fdff00">' ?>
                <?php else: ?>
                <?= '<tr>' ?>
                <?php endif; ?>
                    <td class="align-center"><?= $id?></td>
                    <td><?php echo $j->name;?></td>
                    <td><?php echo $j->email;?></td>
                    <td><?php echo $j->message;?></td>
                    <td>
                        <!--<a href="#" class="table-icon archive" title="Archive"></a>-->
                        <?php if($session_id == 1 AND $username === 'admin'): ?>
                            <a href="/admin/mess_delete/<?php echo $j->id_message; ?>" class="table-icon delete" title="Delete"></a>
                        <?php endif; ?>

                        <?php if($j->status === 'new'): ?>
                            <a href="/admin/mess_read/<?php echo $j->id_message; ?>" title="Read" style="color: #000000"><u>Read a</u></a>
                        <?php endif; ?>
                    </td>
                </tr>
                <?php $id++; ?>
            <?php endforeach ?>
            <?php else: echo "<div class='n_warning'><p><b>no messages</b></p></div>";?>
        <?php endif;?>
        </tr>
        </tbody>
    </table>
</div>
</div>