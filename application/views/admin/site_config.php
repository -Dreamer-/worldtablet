<div id="main">
    <div class="full_w">
        <div class="h_title">Add new accounts</div>
        <?php if(validation_errors()): ?>
            <div class="n_error"><?php echo validation_errors(); ?></div>
            <div class="sep"></div>
        <?php elseif(!empty($warning)): ?>
            <div class="n_error"><p><?php echo $warning; ?></p></div>
            <div class="sep"></div>
        <?php endif; ?>

        <form action="" method="post" enctype="multipart/form-data">
            <div class="element">
                <label for="attach">Main title site: </label>
                <input type="text" name="name" value="<?= $site_name->value; ?>" style="width: 200px"><br />
            </div>
            <div class="element">
                <label for="attach">Phone: </label>
                <input type="text" name="phone" style="width: 200px"><br /><br />
            </div>
            <div class="element">
                <label for="attach">Site logo</label>
                <img src="/img/timthumb.php?src=/logo/<?= $site_logo->value ?>&w=130" style="border: 1px solid; border-radius: 7px;"/><br /><br />
                <input type="file" name="userfile">
            </div>
            <div class="element">
                <button type="submit" class="ok">Ok</button>
            </div>
        </form>
    </div>
</div>
</div>