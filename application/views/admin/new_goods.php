<div id="main">
    <div class="full_w">
        <div class="h_title">Add new goods - form elements</div>
        <?php if(!empty(validation_errors())): ?>
            <div class="n_error"><?php echo validation_errors(); ?></div>
        <?php elseif(!empty($warning)): ?>
            <div class="n_error"><?php echo $warning; ?></div>
        <?php endif; ?>
        <form action="" method="post" enctype="multipart/form-data">
            <div class="element">
                <label for="title">Title <span>(required)</span></label>
                <input id="title" name="title" class="text" />
            </div>
            <div class="element">
                <label for="category">Category <span class="red">(required)</span></label>
                <select name="category" class="err">
                    <option value="">-- select category</option>
                    <?php foreach($categories as $c): ?>
                        <option value="<?php echo $c->categ_id ?>"><?php echo $c->title ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="element">
                <label for="content">Price $<input id="title" name="price" class="text" style="width: 30px; height: 8px" value="<?= set_value('price'); ?>" /></label>
            </div>
            <div class="element">
                <label for="best"><input type="checkbox" name="best" value="1" /> View on home page in slide</label>
            </div>
            <div class="element">

                <label for="content">Text <span>(required)</span></label>
                <textarea name="text" class="textarea" rows="10"><?= set_value('text'); ?></textarea>

                <button form="javascript:void(null)" id="open_specification" style="display: block; margin-top: 10px;" onclick="clickSpecification()">+ Specification</button>

                <div id="specification" style="position: relative; right: 22px; display: none;" >
                    <table>
                        <tr>
                            <td>Производитель</td>
                            <td><input id="specific" name="manufacturer" class="text" /></td>
                        </tr>
                        <tr>
                            <td>Модель</td>
                            <td><input id="specific" name="model" class="text" /></td>
                        </tr>
                        <tr>
                            <td>Дисплей</td>
                            <td><input id="specific" name="display" class="text" /></td>
                        </tr>
                        <tr>
                            <td>Память</td>
                            <td><input id="specific" name="memory" class="text" /></td>
                        </tr>
                        <tr>
                            <td>Связь</td>
                            <td><input id="specific" name="relations" class="text" /></td>
                        </tr>
                        <tr>
                            <td>Размеры</td>
                            <td><input id="specific" name="sizes" class="text" /></td>
                        </tr>
                        <tr>
                            <td>Слоты</td>
                            <td><input id="specific" name="slots" class="text" /></td>
                        </tr>
                        <tr>
                            <td>Процессор</td>
                            <td><input id="specific" name="processor" class="text" /></td>
                        </tr>
                        <tr>
                            <td>Операционная система</td>
                            <td><input id="specific" name="system" class="text" /></td>
                        </tr>
                        <tr>
                            <td>Батарея</td>
                            <td><input id="specific" name="battery" class="text" /></td>
                        </tr>
                        <tr>
                            <td>Камера</td>
                            <td><input id="specific" name="camera" class="text" /></td>
                        </tr>
                        <tr>
                            <td>Вес</td>
                            <td><input id="specific" name="weight" class="text" /></td>
                        </tr>
                        <tr>
                            <td>Прочее</td>
                            <td><input id="specific" name="other" class="text" /></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="element">
                <label for="attach">Image</label>
                <input type="file" name="userfile">
            </div>
            <div class="entry">
                <button type="submit" class="ok">Save page</button> <!--<button class="cancel" href="/admin/pages">Cancel</button>-->
            </div>
        </form>
    </div>
</div>