<div class="container-fluid">
    <div class="side-body">
        <div class="page-title">
            <span class="title">Form UI Kits</span>
            <div class="description">A ui elements use in form, input, select, etc.</div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <div class="title">Form Elements</div>
                        </div>
                    </div>
                    <div class="card-body">
                        <?php if(!empty(validation_errors())): ?>
                            <div class="n_error"><?= validation_errors(); ?></div>
                        <?php elseif(!empty($warning)): ?>
                            <div class="n_error"><?= $warning; ?></div>
                        <?php endif; ?>
                        <form action="" method="post" enctype="multipart/form-data">
                            <div class="sub-title">Title <span>(required)</span></div>
                            <div>
                                <input id="title" name="title" class="form-control" value="<?= $content->goods_title ?>" />
                            </div>

                            <div class="sub-title">Category <span class="red">(required)</span></div>
                            <div>
                                <select name="category">
                                    <option value="<?= $content->categ_id ?>">Default</option>
                                    <?php foreach($categories as $c): ?>
                                        <option value="<?= $c->categ_id ?>"><?= $c->title ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="sub-title">Price <span class="red">(required)</span></div>
                            <div class="form-inline">
                                <div class="form-group">
                                    <label for="exampleInputName2">$</label>
                                    <input class="form-control" id="exampleInputName2" name="price" value="<?= $content->price ?>"/>
                                </div>
                            </div>

                            <div class="sub-title">View on home page in slide</div>
                            <div>
                                <input type="checkbox" class="toggle-checkbox" name="best" value="1" id="best" <?php if(!empty($checked)) {echo $checked;} ?> />
                            </div>

                            <div class="sub-title">Text <span>(required)</span></div>
                            <div>
                                <textarea name="text" class="form-control" rows="10"><?= $content->text ?></textarea>
                            </div>

                                <button form="javascript:void(null)" id="open_specification" class="btn btn-primary" style="display: block; margin-top: 10px;" onclick="clickSpecification()">+ Specification</button>

                                <div id="specification" style="position: relative; right: 22px; display: none; margin-left: 40px" >
                                    <table>
                                        <tr>
                                            <td>Производитель</td>
                                            <td><input id="specific" name="manufacturer" class="text form-control" value="<?= $content->manufacturer ?>" /></td>
                                        </tr>
                                        <tr>
                                            <td>Модель</td>
                                            <td><input id="specific" name="model" class="text form-control" value="<?= $content->model ?>" /></td>
                                        </tr>
                                        <tr>
                                            <td>Дисплей</td>
                                            <td><input id="specific" name="display" class="text form-control" value="<?= $content->display ?>" /></td>
                                        </tr>
                                        <tr>
                                            <td>Память</td>
                                            <td><input id="specific" name="memory" class="text form-control" value="<?= $content->memory ?>" /></td>
                                        </tr>
                                        <tr>
                                            <td>Связь</td>
                                            <td><input id="specific" name="relations" class="text form-control" value="<?= $content->relations ?>" /></td>
                                        </tr>
                                        <tr>
                                            <td>Размеры</td>
                                            <td><input id="specific" name="sizes" class="text form-control" value="<?= $content->sizes ?>" /></td>
                                        </tr>
                                        <tr>
                                            <td>Слоты</td>
                                            <td><input id="specific" name="slots" class="text form-control" value="<?= $content->slots ?>" /></td>
                                        </tr>
                                        <tr>
                                            <td>Процессор</td>
                                            <td><input id="specific" name="processor" class="text form-control" value="<?= $content->processor ?>" /></td>
                                        </tr>
                                        <tr>
                                            <td>Операционная система</td>
                                            <td><input id="specific" name="system" class="text form-control" value="<?= $content->system ?>" /></td>
                                        </tr>
                                        <tr>
                                            <td>Батарея</td>
                                            <td><input id="specific" name="battery" class="text form-control" value="<?= $content->battery ?>" /></td>
                                        </tr>
                                        <tr>
                                            <td>Камера</td>
                                            <td><input id="specific" name="camera" class="text form-control" value="<?= $content->camera ?>" /></td>
                                        </tr>
                                        <tr>
                                            <td>Вес</td>
                                            <td><input id="specific" name="weight" class="text form-control" value="<?= $content->weight ?>" /></td>
                                        </tr>
                                        <tr>
                                            <td>Прочее</td>
                                            <td><input id="specific" name="other" class="text form-control" value="<?= $content->other ?>" /></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <div class="element">
                                <label for="attach">Image</label>
                                <img src="/upload/timthumb.php?src=/goods/<?= $content->image ?>&w=80&h=70"style="border-radius: 5px"/><br /><br />
                                <input type="file" name="userfile">
                            </div>
                            <div class="element">
                                <button type="submit" class="ok">Save page</button> <!--<button class="cancel" href="/admin/pages">Cancel</button>-->
                            </div>
                            <?php if(!empty($get_comment)):?>
                                <div class="element">
                                    <label for="attach">Comments:</label>
                                    <?php foreach($get_comment as $j): ?>
                                        <div id="comment">
                                            <label for="name_and_data" style="font-style: italic"><?= strip_tags($j->name); ?>, <?= $j->date; ?>: <a href="/admin/comment/delete/<?= $j->product_id.'/'.$j->id ?>" id="delete">Delete</a></label>
                                            <label for="comment" style="font-size: 15px"><?= strip_tags($j->comment); ?></label>
                                        </div>
                                    <?php endforeach;?>
                                </div>
                            <?php endif; ?>
                        </form>
                    </div>
    </div>
</div>