<div id="main">
    <div class="full_w">
        <div class="h_title">Manage orders - table</div>

        <?php if(!empty($customer)): ?>
        <table>
            <thead>
            <tr>
                <th scope="col" style="width: 7px;">ID Order</th>
                <th scope="col" style="width: 20px;">Date</th>
                <th scope="col" style="width: 10px;">Status</th>
                <th scope="col" style="width: 7px;">Modify</th>
            </tr>
            </thead>

            <tbody>
            <?php foreach($customer as $key => $j): ?>
                <tr>
                    <td class="align-center"><a href="/admin/order/<?= $j->id_order; ?>"><?php echo $j->id_order;?></a></td>
                    <td><a href="/admin/order/<?= $j->id_order; ?>"><?php echo $j->date;?></a></td>
                    <td class="align-center"><a href="/admin/order/<?= $j->id_order; ?>"><?php echo $j->status;?></a></td>
                    <td>
                        <a href="/admin/order_delete/<?php echo $j->id_order;?>" class="table-icon delete" title="Delete"></a>
                    </td>
                </tr>
            <?php endforeach ?>
            <?php else: echo "<div class='n_warning'><p><b>No orders</b></p></div>";?>
            <?php endif;?>
            </tr>
            </tbody>
        </table>
    </div>
</div>