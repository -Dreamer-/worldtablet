<div id="main">
    <div class="full_w">
        <div class="order">
            <div class="h_title">Order [<?=$customer->status?>]</div>
            <form method="post" action="" name="order">
                <div class="element">
                    <label for="title" style="font-size: 17px">Information about the customer:</label>
                    <div class="entry">Name: <b><?= $customer->name; ?></b></div>
                    <div class="entry">Telephone: <b><?= $customer->telephone; ?></b></div>
                    <div class="entry">Email: <b><?= $customer->email; ?></b></div><br />
                </div>

                <div class="element">
                    <label for="title" style="font-size: 17px">Information about ordering:</label>
                    <?php if (!empty($order)): ?>
                        <?php $x = 1;?>
                        <?php foreach ($order as $j):?>
                            <div class="element">
                                <label for="title" style="font-size: 12px">Product #<?=$x;?></label>
                                <div class="entry">ID of the product: <b><?= $j->id_goods; ?></b></div>
                                <div class="entry">Goods title:
                                    <b>
                                            "<?=$j->title_goods;?>"<br />
                                    </b>
                                </div>
                                <div class="entry">Quantity: <b><?= $j->qty_goods; ?></b></div>
                                <div class="entry">Price total: <b>$<?= $j->price_goods; ?></b></div><br />
                            </div>
                            <?php $x++;?>
                        <?php endforeach;?>
                    <?php else: echo "error";?>
                    <?php endif?>
                </div>

                <div class="element">
                    <div class="entry">Order date: <b><?= $customer->date; ?></b></div>
                    <div class="entry">Order status:
                        <select name="status" onchange="document.forms['order'].submit()">
                            <option value="<?= $customer->status; ?>"><?= $customer->status; ?></option>
                            <option value="New" style="color: #ff0000;">New</option>
                            <option value="Considered" style="color: #0000FF;">Considered</option>
                            <option value="Sold" style="color: #00A100;">Sold</option>
                            <option value="Canceled" style="color: #670000;">Canceled</option>
                        </select>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>