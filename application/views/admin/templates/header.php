<!DOCTYPE html>
<html>
<head>
    <title><?php echo $page_title, ' - ', $page_name ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1" charset="utf-8">
    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300,400' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>
    <!-- CSS Libs -->
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="/css/bootstrap-switch.min.css">
    <link rel="stylesheet" type="text/css" href="/css/checkbox3.min.css">
    <link rel="stylesheet" type="text/css" href="/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="/css/dataTables.bootstrap.css">
    <link rel="stylesheet" type="text/css" href="/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="/css/development.css">
    <!-- CSS App -->
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <link rel="stylesheet" type="text/css" href="/css/themes/flat-blue.css">
</head>
<body class="flat-blue">
    <div class="app-container">
        <div class="row content-container">
            <nav class="navbar navbar-default navbar-fixed-top navbar-top">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-expand-toggle">
                            <i class="fa fa-bars icon"></i>
                        </button>
                        <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
                            <i class="fa fa-th icon"></i>
                        </button>
                    </div>
                    <?php if(empty($username)) {echo 'not found';}?>
                    <ul class="nav navbar-nav navbar-right">
                        <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
                            <i class="fa fa-times icon"></i>
                        </button>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-comments-o"></i></a>
                            <ul class="dropdown-menu animated fadeInDown">
                                <li class="title">
                                    Notification <span class="badge pull-right">0</span>
                                </li>
                                <li class="message">
                                    No new notification
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown danger">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-star-half-o"></i> 4</a>
                            <ul class="dropdown-menu danger animated fadeInDown">
                                <li class="title">
                                    Notification <span class="badge pull-right">4</span>
                                </li>
                                <li>
                                    <ul class="list-group notifications">
                                        <a href="#">
                                            <li class="list-group-item">
                                                <span class="badge">1</span> <i class="fa fa-exclamation-circle icon"></i> new registration
                                            </li>
                                        </a>
                                        <a href="#">
                                            <li class="list-group-item">
                                                <span class="badge success">1</span> <i class="fa fa-check icon"></i> new orders
                                            </li>
                                        </a>
                                        <a href="#">
                                            <li class="list-group-item">
                                                <span class="badge danger">2</span> <i class="fa fa-comments icon"></i> customers messages
                                            </li>
                                        </a>
                                        <a href="#">
                                            <li class="list-group-item message">
                                                view all
                                            </li>
                                        </a>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown profile">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Emily Hart <span class="caret"></span></a>
                            <ul class="dropdown-menu animated fadeInDown">
                                <li class="profile-img">
                                    <img src="/img/profile/picjumbo.com_HNCK4153_resize.jpg" class="profile-img">
                                </li>
                                <li>
                                    <div class="profile-info">
                                        <h4 class="username">Emily Hart</h4>
                                        <p>emily_hart@email.com</p>
                                        <div class="btn-group margin-bottom-2x" role="group">
                                            <button type="button" class="btn btn-default"><i class="fa fa-user"></i> Profile</button>
                                            <button type="button" class="btn btn-default"><i class="fa fa-sign-out"></i> Logout</button>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>

            <div class="side-menu sidebar-inverse">
                <nav class="navbar navbar-default" role="navigation">
                    <div class="side-menu-container">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="#">
                                <div class="icon">WT</div>
                                <div class="title">Admin</div>
                            </a>
                            <button type="button" class="navbar-expand-toggle pull-right visible-xs">
                                <i class="fa fa-times icon"></i>
                            </button>
                        </div>
                        <ul class="nav navbar-nav">
                            <li>
                                <a href="/admin/home"><span class="icon fa fa-home"></span><span class="title">Home</span></a>
                            </li>
                            <li>
                                <a href="/admin/pages"><span class="icon fa fa-files-o"></span><span class="title">Pages</span></a>
                            </li>
                            <li>
                                <a href="/admin/goods"><span class="icon fa fa-table"></span><span class="title">Goods</span></a>
                            </li>
                            <li>
                                <a href="/admin/categories"><span class="icon glyphicon glyphicon-tags"></span><span class="title">Categories</span></a>
                            </li>
                            <li>
                                <a href="/admin/messages"><span class="icon fa fa-envelope-o"></span><span class="title">Messages<?php if($count_mess != 0) {echo '<font class="messages"><b id="mess">+'.$count_mess.'</b></font>';} ?></span></a>
                            </li>
                            <li>
                                <a href="/admin/orders"><span class="icon glyphicon glyphicon-list-alt"></span><span class="title">Orders</span></a>
                            </li>
                            <!--<li class="
                            b1"><a class="icon photo" href="/admin/images">Show images for slide</a></li>-->
                            <li>
                                <a href="/admin/config"><span class="icon fa fa-cogs"></span><span class="title">Site config</span></a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </nav>
            </div>
                <?php if($session_id == 1 AND $username === 'admin'): ?>
                    <div class="box">
                        <div class="h_title">&#8250; Users</div>
                        <ul>
                            <li class="b1"><a class="icon users" href="/admin/users">Show all users</a></li>
                            <li class="b2"><a class="icon add_user" href="/admin/create_user">Add new user</a></li>
                            <!--<li class="b1"><a class="icon block_users" href="">Lock users</a></li>-->
                        </ul>
                    </div>
                <?php endif; ?>
                <!--<div class="box">
                    <div class="h_title">&#8250; Manage content</div>
                    <ul>
                        <li class="b2"><a class="icon report" href="">Reports</a></li>
                        <li class="b2"><a class="icon add_page" href="">Add new page</a></li>
                    </ul>
                </div>-->
            </div>