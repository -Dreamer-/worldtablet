<footer class="app-footer">
    <div class="wrapper">
        <span class="pull-right">2.1 <a href="#"><i class="fa fa-long-arrow-up"></i></a></span> © 2015 Copyright. <a href="https://plus.google.com/u/0/103399909738350117110/">Makarenko Nikita</a>
    </div>
</footer>
    <!-- Javascript Libs -->
    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/Chart.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap-switch.min.js"></script>

    <script type="text/javascript" src="/js/jquery.matchHeight-min.js"></script>
    <script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/select2.full.min.js"></script>
    <script type="text/javascript" src="/js/ace/ace.js"></script>
    <script type="text/javascript" src="/js/ace/mode-html.js"></script>
    <script type="text/javascript" src="/js/ace/theme-github.js"></script>
    <!-- Javascript -->
    <script type="text/javascript" src="/js/app.js"></script>
    <script type="text/javascript" src="/js/main.js"></script>
    </body>

    </html>