<div id="main">
    <div class="full_w">
        <div class="h_title">Manage categories - table</div>

        <?php if(!empty($user)): ?>
        <table>
            <thead>
            <tr>
                <th scope="col" style="width: 45px;">ID</th>
                <th scope="col">Username</th>
                <th scope="col">Password</th>
                <th scope="col" style="width: 55px;">Modify</th>
            </tr>
            </thead>

            <tbody>
            <?php foreach($user as $j): ?>
                <tr>
                    <td class="align-center"><?php echo $j->id;?></td>
                    <td><?php echo $j->username;?></td>
                    <td><?php echo $j->password;?></td>
                    <td>
                        <a href="/admin/user_edit/<?php echo $j->id;?>" class="table-icon edit" title="Edit"></a>
                        <!--<a href="#" class="table-icon archive" title="Archive"></a>-->
                        <a href="/admin/user_delete/<?php echo $j->id;?>" class="table-icon delete" title="Delete"></a>
                    </td>
                </tr>
            <?php endforeach ?>
            <?php else: echo "<div class='n_warning'><p><b>no categories</b></p></div>";?>
            <?php endif;?>
            </tr>
            </tbody>
        </table>
        <div class="entry">

            <div class="sep"></div>
            <?php if(validation_errors()): ?>
                <div class="n_error"><?php echo validation_errors(); ?></div>
                <div class="sep"></div>
            <?php elseif(!empty($error_pass)): ?>
                <div class="n_error"><p><?php echo $error_pass; ?></p></div>
                <div class="sep"></div>
            <?php endif; ?>

            <form method="post" action="">
                <label for="title">Add new accounts:</label>
                <br />Username: <br />
                <input type="text" name="username" style="width: 200px"><br />
                <br />Password: <br />
                <input type="password" name="password" style="width: 200px"><br />
                <br />Repeat password: <br />
                <input type="password" name="repeat" style="width: 200px"><br /><br />
                <button type="submit" class="ok">Add</button>
            </form>
        </div>
    </div>
</div>