<div class="container-fluid">
    <div class="side-body">
        <div class="page-title">
            <span class="title">Table</span>
            <div class="description">A bootstrap table for display list of data.</div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="card">
                    <div class="card-header">

                        <div class="card-title">
                            <div class="title">Товары</div>
                        </div>
                    </div>

                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Link</th>
                                    <th style="width: 65px;">Modify</th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php if(!empty($get_admin_menu)): ?>
                                    <?php foreach($get_admin_menu as $key => $j): ?>
                                        <tr>
                                            <td class="align-center"><?php echo $j->id;?></td>
                                            <td><?php echo $j->title;?></td>
                                            <td><?php echo $j->link;?></td>
                                            <td>
                                                <a href="/admin/page_edit/<?php echo $j->id; ?>" class="icon fa fa-edit" title="Edit"></a>
                                                <!--<a href="#" class="table-icon archive" title="Archive"></a>-->
                                                <a href="/admin/page_delete/<?php echo $j->id; ?>" class="icon fa fa-trash" title="Delete"></a>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                <?php else: echo "error";?>
                                <?php endif;?>
                                </tr>
                            </tbody>
                        </table>
                        <div class="entry">
                            <!--<div class="pagination">
                                <span>« First</span>
                                <span class="active">1</span>
                                <a href="">2</a>
                                <a href="">3</a>
                                <a href="">4</a>
                                <span>...</span>
                                <a href="">23</a>
                                <a href="">24</a>
                                <a href="">Last »</a>
                            </div>-->
                            <div class="sep"></div>
                            <a class="btn btn-info" href="/admin/new_page">Add new page</a>
                            <a class="btn btn-default" href="/admin/categories">Categories</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>