<link rel="stylesheet" href="/css/style.css" xmlns="http://www.w3.org/1999/html">

<div>
    <?php if ($content->link == 'home'): ?>
        <div class="homepage-slider">
            <div id="sequence">
                <?php if(!empty($get_image)): ?>
                    <ul class="sequence-canvas">
                        <?php $n = 1; ?>
                        <?php foreach($get_image AS $j): ?>
                                <!-- Slide 1 -->
                                <li class="bg<?= $n; ?>">
                                    <!-- Slide Title -->
                                    <h2 class="title"><a href="/product/<?= $j->goods_id; ?>" id="slide_link"><?= $j->goods_title; ?></a></h2>
                                    <!-- Slide Text -->
                                    <h3 class="subtitle">
                                        <?= mb_substr($j->text, 0, 160); ?>...<br />
                                        Цена: <div style="display: inline; font-size: 19px">$<?= $j->price; ?></div><br />
                                        <br /><a href="/product/<?= $j->goods_id; ?>" style="color: #ffffff">Подробнее...</a>
                                    </h3>
                                    <!-- Slide Image -->
                                    <img class="slide-img" src="/upload/timthumb.php?src=/goods/<?= $j->image; ?>&h=250" alt="Slide <?= $n; ?>" style="margin-left: 85px" />
                                </li>
                                <!-- End Slide 1 -->
                            <?php $n++; ?>
                        <?php endforeach; ?>
                    </ul>
                    <div class="sequence-pagination-wrapper">
                        <ul class="sequence-pagination">
                            <?php for($j = 1; $j < $n; $j++): ?>
                                <li><?= $j; ?></li>
                            <?php endfor; ?>
                        </ul>
                    </div>
                <?php else: echo 'not fount'; ?>
                <?php endif; ?>
            </div>
        </div>

        <div class="section home">
            <div class="container">
                <h2><?= ($lang == 'ru') ? 'Смартфоны' : 'Smartphones'; ?> <a href="/goods/1"><?= ($lang == 'ru') ? 'Смотреть все' : 'View all'; ?><span style="font-size: 13px; color: #9fbb63" class="glyphicon glyphicon-chevron-right"></span></a></h2>
                <div class="row">
                    <?php foreach($new_in_site['phone'] as $news): ?>
                        <div class="col-md-3 col-sm-6">
                            <div class="portfolio-item">
                                <div class="portfolio-image">
                                    <a href="page-portfolio-item.html" style="max-width: 100px">
                                        <div class="ribbon-wrapper">
                                            <div class="price-ribbon ribbon-green"><?= ($lang == 'ru') ? 'Новинка!' : 'New!'; ?></div>
                                        </div>
                                        <img src="/upload/timthumb.php?src=/goods/<?= $news->image; ?>&h=244" alt="Project Name">
                                    </a>
                                </div>
                                <div class="portfolio-info-fade">
                                    <ul>
                                        <li class="portfolio-project-name"><?= $news->goods_title; ?></li>
                                        <li><?= mb_substr($news->text, 0, 35); ?>...</li>
                                        <li><?= ($lang == 'ru') ? 'Цена' : 'Price'; ?>: $<?= $news->price ?></li>
                                        <li class="read-more"><a href="/product/<?= $news->goods_id; ?>" class="btn"><?= ($lang == 'ru') ? 'Подробнее' : 'More'; ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="container">
                <h2><?= ($lang == 'ru') ? 'Планшеты' : 'Tablets'; ?><a href="/goods/2"><?= ($lang == 'ru') ? 'Смотреть все' : 'View all'; ?><span style="font-size: 13px; color: #9fbb63" class="glyphicon glyphicon-chevron-right"></span></a></h2>
                <div class="row">
                    <?php foreach($new_in_site['tablet'] as $news): ?>
                        <div class="col-md-3 col-sm-6">
                            <div class="portfolio-item">
                                <div class="portfolio-image">
                                    <a href="page-portfolio-item.html" style="max-width: 100px">
                                        <div class="ribbon-wrapper">
                                            <div class="price-ribbon ribbon-blue"><?= ($lang == 'ru') ? 'Новинка!' : 'New!'; ?></div>
                                        </div>
                                        <img src="/upload/timthumb.php?src=/goods/<?= $news->image; ?>&h=244" alt="Project Name">
                                    </a>
                                </div>
                                <div class="portfolio-info-fade">
                                    <ul>
                                        <li class="portfolio-project-name"><?= $news->goods_title; ?></li>
                                        <li><?= mb_substr($news->text, 0, 35); ?>...</li>
                                        <li>Цена: $<?= $news->price ?></li>
                                        <li class="read-more"><a href="/product/<?= $news->goods_id; ?>" class="btn"><?= ($lang == 'ru') ? 'Подробнее' : 'More'; ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <div class="section">
        <div class="container">
            <?= nl2br($content->text); ?>
            <?php if($content->link == 'about'): ?>
            <div class="col-sm-6">
                <div class="video-wrapper">
                    <iframe src="https://www.youtube.com/embed/LBplAn1uv7Y" width="500" height="281" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>
                </div>
            </div>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<!--<a href="/goods" class="link" style="color: orange;">товарах</a>-->