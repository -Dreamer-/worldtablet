<div class="section">
    <div class="container">
        <?php if(!empty($goods)): ?>
            <div class="row">
                <!-- Product Image & Available Colors -->
                <div class="col-sm-6">
                    <div class="product-image-large" style="display: inline-block">
                        <a href="/upload/goods/<?= $goods->image;?>"><img src="/upload/timthumb.php?src=/goods/<?= $goods->image;?>&w=539" style="max-height: 480px"></a>
                    </div>
                </div>
                <!-- End Product Image & Available Colors -->
                <!-- Product Summary & Options -->
                <div class="col-sm-6 product-details">
                    <h4><?= $goods->goods_title?></h4>
                    <div class="price" style="display: inline-block">
                        <span class="price-was" style="text-decoration: none;">$<?= $goods->price; ?></span> <!--$999.99-->
                    </div>
                    <!--<h5>Категория:</h5> <p style="display: inline"><a href="/goods/<?=$goods->categ_id;?>" style="display: inline;"><?= $goods->title?></a></p>-->
                    <h5>Quick Overview</h5>
                    <p>
                        Здесь я буду брать из БД краткое описание товара.
                    </p>
                    <table class="shop-item-selections">
                        <form method="post" action='javascript:void(null);' onsubmit='sendToCart()' id='send_to_cart' name="send_to_cart">
                            <input type="hidden" name="form_type" value="buy">
                            <input type="hidden" name="goods_id" value="<?= $goods->goods_id; ?>">
                            <!-- Quantity -->
                            <tr>
                                <td><b>Quantity:</b></td>
                                <td>
                                    <input type="text" value="1" name="qty" maxlength="2" class="form-control input-sm input-micro">
                                </td>
                            </tr>
                            <!-- Add to Cart Button -->
                            <tr>
                                <td>&nbsp;</td>
                                <td>
                                    <button type="submit" form="send_to_cart" class="btn btn"><i class="icon-shopping-cart icon-white"></i> Add to Cart</button>
                                </td>
                            </tr>
                        </form>
                    </table>

                    <br />
                    <?php if(form_error('qty')): ?>
                        <div class="alert alert-danger" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Error:</span>
                            <?= form_error('qty'); ?>
                        </div>
                    <?php endif; ?>
                    <button onclick="window.history.back();" class=".btn-grey">Назад</button>
                </div>
            </div>
    </div>

    <div class="container">
        <div class="row">
            <!-- Full Description & Specification -->
            <div class="col-sm-12">
                <div class="tabbable">
                    <!-- Tabs -->
                    <ul class="nav nav-tabs product-details-nav">
                        <li class="active"><a href="#tab1" data-toggle="tab">Description</a></li>
                        <li><a href="#tab2" data-toggle="tab">Specification</a></li>
                    </ul>
                    <!-- Tab Content (Full Description) -->
                    <div class="tab-content product-detail-info">
                        <div class="tab-pane active" id="tab1">
                            <h4>Поговорим о <?= $goods->goods_title?></h4>
                            <?= nl2br($goods->text)?>
                        </div>
                        <!-- Tab Content (Specification) -->
                        <div class="tab-pane" id="tab2">
                            <table>
                                <tr>
                                    <td>Производитель</td>
                                    <td><?= $goods->manufacturer ?></td>
                                </tr>
                                <tr>
                                    <td>Модель</td>
                                    <td><?= $goods->model ?></td>
                                </tr>
                                <tr>
                                    <td>Дисплей</td>
                                    <td><?= $goods->display ?></td>
                                </tr>
                                <tr>
                                    <td>Память</td>
                                    <td><?= $goods->memory ?></td>
                                </tr>
                                <tr>
                                    <td>Связь</td>
                                    <td><?= $goods->relations ?></td>
                                </tr>
                                <tr>
                                    <td>Размеры</td>
                                    <td><?= $goods->sizes ?></td>
                                </tr>
                                <tr>
                                    <td>Слоты</td>
                                    <td><?= $goods->slots ?></td>
                                </tr>
                                <tr>
                                    <td>Процессор</td>
                                    <td><?= $goods->processor ?></td>
                                </tr>
                                <tr>
                                    <td>Операционная система</td>
                                    <td><?= $goods->system ?></td>
                                </tr>
                                <tr>
                                    <td>Батарея</td>
                                    <td><?= $goods->battery ?></td>
                                </tr>
                                <tr>
                                    <td>Камера</td>
                                    <td><?= $goods->camera ?></td>
                                </tr>
                                <tr>
                                    <td>Вес</td>
                                    <td><?= $goods->weight ?></td>
                                </tr>
                                <tr>
                                    <td>Прочее</td>
                                    <td><?= $goods->other ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Full Description & Specification -->


    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default" style="margin-top: 20px">
                    <div class="panel-heading panel-heading" style="background-color: #ffffff;">Рекомендуемые товары:</div>
                    <div class="panel-body" style="background-color: #f0f0f0;">
                        <div class="products-slider">
                            <?php if(!empty($recommended)): ?>
                                <?php foreach($recommended as $j): ?>
                                    <?php if($goods->goods_id !== $j->goods_id): ?>
                                        <!-- Products Slider Item -->
                                        <div class="shop-item">
                                            <!-- Product Image -->
                                            <div class="image">
                                                <a href="/product/<?= $j->goods_id; ?>" style="max-width: 265px; text-align: center; display: inline-block; margin-top: 4px;"><img src="/upload/timthumb.php?src=/goods/<?= $j->image;?>&h=159"></a>
                                            </div>
                                            <!-- Product Title -->
                                            <div class="title" style="height: 31px; position: relative; top: -20px;">
                                                <h3><a href="/product/<?= $j->goods_id; ?>">&nbsp;<?= $j->goods_title; ?></a></h3>
                                            </div>
                                            <!-- Product Price -->
                                            <div class="price">
                                                $<?= $j->price; ?>
                                            </div>
                                            <!-- Buy Button -->
                                            <div class="actions">
                                                <a href="/product/<?= $j->goods_id; ?>" class="btn btn-small"><i class="icon-shopping-cart icon-white"></i> Подробнее...</a>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container commentaries">
        <div class="row">
            <div class="col-md-12">
                <div class="post-coments">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h4>Отзывы (<?= $count_comment ?>)</h4></div>
                        <div class="panel-body">
                        <?php if(!empty($get_comment)):?>
                            <ul class="post-comments">
                                <?php foreach($get_comment as $j): ?>
                                    <li>
                                        <div class="comment-wrapper">
                                            <div class="comment-author"><?= strip_tags($j->name); ?></div>
                                            <p>
                                                <?= strip_tags($j->comment); ?>
                                            </p>
                                            <!-- Comment Controls -->
                                            <div class="comment-actions">
                                                <span class="comment-date"><?= $j->date; ?></span>
                                                <!-- TODO - В будущем доделать :)
                                                    <a href="#" data-toggle="tooltip" data-original-title="Vote Up" class="show-tooltip"><i class="glyphicon glyphicon-thumbs-up"></i></a>
                                                    <a href="#" data-toggle="tooltip" data-original-title="Vote Down" class="show-tooltip"><i class="glyphicon glyphicon-thumbs-down"></i></a>
                                                    <span class="label label-success">+8</span>
                                                    <a href="#" class="btn btn-micro btn-grey comment-reply-btn"><i class="glyphicon glyphicon-share-alt"></i> Reply</a>
                                                -->
                                            </div>
                                        </div>
                                    </li>
                                <?php endforeach;?>
                            </ul>
                            <?php else: ?>
                                <h3 style="vertical-align: middle; text-align: center; position: relative; display: block; height: 50px">К сожалению отзывов еще никто не оставлял. Станте первыми!</h3>
                            <?php endif; ?>
                        <!-- Pagination -->
                        <!-- TODO - В будущем доделать :)
                        <div class="pagination-wrapper ">
                            <ul class="pagination pagination-sm">
                                <li class="disabled"><a href="#">Previous</a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#">Next</a></li>
                            </ul>
                        </div>
                        -->
                            <h4 style="border-top: 2px dotted #CCC;"></h4>
                            <div class="comment-form-wrapper">
                                <form method="post" class="addComment" action='javascript:void(null)' onsubmit="addComment(<?= $goods->goods_id; ?>)" style="margin-top: 10px; margin-bottom: 10px">
                                    <input type="hidden" name="form_type" value="comment">

                                    <div class="form-group">
                                        <label for="comment-name"><i class="glyphicon glyphicon-user"></i> <b>Ваше имя</b></label>
                                        <font id="error"><?= form_error('name'); ?></font>
                                        <input class="form-control" id="comment-name" type="text" name="name" value="<?= set_value('name');?>"><br /><br />
                                    </div>

                                    <!--<div class="form-group">
                                        <label for="comment-email"><i class="glyphicon glyphicon-envelope"></i> <b>Your Email</b></label>
                                        <input class="form-control" id="comment-email" type="text" placeholder="">
                                    </div>-->

                                    <div class="form-group">
                                        <label for="comment-message"><i class="glyphicon glyphicon-comment"></i> <b>Ваш коментарий</b></label>
                                        <font id="error"><?= form_error('comment'); ?></font>
                                        <textarea class="form-control" rows="5" id="comment-message" name="comment"><?= set_value('comment');?></textarea>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-large pull-right" name="sendComment">Отправить</button>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif;?>
        </div>
    </div>
</div>