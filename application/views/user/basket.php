<div class="section">
    <div class="container basket-cart">
        <?php if(!empty($this->cart->contents())): ?>
            <div class="row">
                <div class="col-md-12">
                    <!-- Shopping Cart Items -->
                    <table class="shopping-cart">
                        <!-- Shopping Cart Item -->
                        <?php foreach($this->cart->contents() as $item): ?>
                            <form method="post" name="cart" action="/cart/edit/<?=$item['rowid'];?>">
                                <!-- Shopping Cart Item -->
                                <tr>
                                    <!-- Shopping Cart Item Image -->
                                    <td class="image"><a href="/product/<?= $data[$item['id']]->goods_id?>"><img src="/upload/timthumb.php?src=/goods/<?= $data[$item['id']]->image;?>&h=90" class="images img-it-cart" alt="Item Name"></a></td>
                                    <!-- Shopping Cart Item Description & Features -->
                                    <td>
                                        <div class="cart-item-title">
                                            <a href="/product/<?= $data[$item['id']]->goods_id?>"><?= $data[$item['id']]->goods_title?></a>
                                        </div>
                                    </td>
                                    <!-- Shopping Cart Item Quantity -->
                                    <td class="quantity">
                                        <input class="form-control input-sm input-micro" type="text" name="qty" maxlength="2" value="<?= $item['qty'];?>" onchange="forms['cart'].submit()">
                                    </td>
                                    <!-- Shopping Cart Item Price -->
                                    <td class="price">$<?= $item['price'];?></td>
                                    <!-- Shopping Cart Item Actions -->
                                        <!--<td class="price"><b>$<?// $item['price']*$item['qty'];?></b></td>-->
                                    <td class="actions">
                                        <a href="/cart/delete/<?=$item['rowid'];?>" class="btn btn-xs btn-grey" id="delete"><i class="glyphicon glyphicon-trash"></i></a>
                                    </td>
                                </tr>
                            </form>
                            <!-- End Shopping Cart Item -->
                        <?php endforeach; ?>
                    </table>
                    <!-- End Shopping Cart Items -->
                </div>
            </div>
            <div class="row">
                <!-- Promotion Code -->
                <div class="col-md-4  col-md-offset-0 col-sm-6 col-sm-offset-6">
                    <div class="cart-promo-code">
                        <h6><i class="glyphicon glyphicon-gift"></i> Have a promotion code?</h6>
                        <div class="input-group">
                            <input class="form-control input-sm" id="appendedInputButton" type="text" value="">
                                    <span class="input-group-btn">
                                        <button class="btn btn-sm btn-grey" type="button">Apply</button>
                                    </span>
                        </div>
                    </div>
                </div>
                <!-- Shipment Options -->
                <div class="col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-6">
                    <div class="cart-shippment-options">
                        <h6><i class="glyphicon glyphicon-plane"></i> Shippment options</h6>
                        <div class="input-append">
                            <select class="form-control input-sm">
                                <option value="1">Standard - FREE</option>
                                <option value="2">Next day delivery - $10.00</option>
                            </select>
                        </div>
                    </div>
                </div>
                <!-- Shopping Cart Totals -->
                <div class="col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-6">
                    <table class="cart-totals">
                        <tr>
                            <td><b>Доставка</b></td>
                            <td>Бесплатно</td>
                        </tr>
                        <tr>
                            <td><b>Скидка</b></td>
                            <td>- $0.00</td>
                        </tr>
                        <tr class="cart-grand-total">
                            <td><b>Итого</b></td>
                            <td><b>$<?= $this->cart->total(); ?></b></td>
                        </tr>
                    </table><!-- Action Buttons -->
                    <div class="pull-right">
                        <a href="javascript:void(null)" onclick="cleanBasket()" class="btn btn-grey"><i class="glyphicon glyphicon-remove"></i> Очистить корзину</a>
                        <a href="#" class="btn" id="check_out"><i class="glyphicon glyphicon-shopping-cart icon-white"></i> Оформить заказ</a>
                    </div>
                </div>
            </div>

        <?php elseif(empty($this->cart->contents())): ?>
            <div class="panel panel-info">
                <div class="panel-heading">
                    Упс...
                </div>
                <div class="panel-body" style="text-align: center;">Корзина пуста!</div>
            </div>
        <?php endif; ?>

        <?php if(!empty($this->cart->contents())): ?>
            <div class="row" id="check_out_form" style="display: none;">
                <div class="col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-6" style="text-align: right; float: right; margin-top: 10px; padding-top: 10px; border-top: 2px solid #535b60; width: 390px;">
                    <form method="post" action="">
                        <font id="error"><?= form_error('name'); ?></font>
                        <b>Имя: <input type="text" name="name" class="form-control check_out"  value="<?= set_value('name');?>"><br /><br /></b>

                        <font id="error"><?= form_error('telephone'); ?></font>
                        <b>Телефон: <input type="text" name="telephone" class="form-control check_out" value="<?= set_value('telephone');?>"><br /><br /></b>

                        <font id="error"><?= form_error('email'); ?></font>
                        <b>Email: <input type="email" name="email" class="form-control check_out" value="<?= set_value('email');?>"><br /><br />

                        <button type="submit" id="order" class="btn">Отправить</button>
                    </form>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>