
            <div class="post-coments">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4>Отзывы (<?= $count_comment ?>)</h4></div>
                    <div class="panel-body">
                        <?php if(!empty($get_comment)):?>
                            <ul class="post-comments">
                                <?php foreach($get_comment as $j): ?>
                                    <li>
                                        <div class="comment-wrapper">
                                            <div class="comment-author"><?= strip_tags($j->name); ?></div>
                                            <p>
                                                <?= strip_tags($j->comment); ?>
                                            </p>
                                            <!-- Comment Controls -->
                                            <div class="comment-actions">
                                                <span class="comment-date"><?= $j->date; ?></span>
                                            </div>
                                        </div>
                                    </li>
                                <?php endforeach;?>
                            </ul>
                        <?php else: ?>
                            <h3 style="vertical-align: middle; text-align: center; position: relative; display: block; height: 50px">К сожалению отзывов еще никто не оставлял. Станте первыми!</h3>
                        <?php endif; ?>
                        <h4 style="border-top: 2px dotted #CCC;"></h4>
                        <div class="comment-form-wrapper">
                            <form method="post" class="addComment" action='javascript:void(null)' onsubmit="addComment(<?= $goods->goods_id; ?>)" style="margin-top: 10px; margin-bottom: 10px">
                                <!-- COMMENT NUM.2 -->
                                <input type="hidden" name="form_type" value="comment">

                                <div class="form-group">
                                    <label for="comment-name"><i class="glyphicon glyphicon-user"></i> <b>Ваше имя</b></label>
                                    <font id="error"><?= form_error('name'); ?></font>
                                    <input class="form-control" id="comment-name" type="text" name="name" ><br /><br />
                                </div>
                                <div class="form-group">
                                    <label for="comment-message"><i class="glyphicon glyphicon-comment"></i> <b>Ваш коментарий</b></label>
                                    <font id="error"><?= form_error('comment'); ?></font>
                                    <textarea class="form-control" rows="5" id="comment-message" name="comment"></textarea>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-large pull-right" name="send">Отправить</button>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>