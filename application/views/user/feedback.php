
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-sm-7">
                <!-- Map -->
                <div id="contact-us-map">

                </div>
                <!-- End Map -->
                <!-- Contact Info -->
                <p class="contact-us-details">
                    <b>Address:</b> Kharkov, Ukraine<br/>
                    <b>Email:</b> <a href="mailto:getintoutch@yourcompanydomain.com">getintoutch@yourcompanydomain.com</a>
                </p>
                <!-- End Contact Info -->
            </div>

            <div class="col-sm-5">
                <!-- Contact Form -->
                <h3>Send Us a Message</h3>
                <div class="contact-form-wrapper">
                    <form action="/feedback" method="post" class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="Name" class="col-sm-3 control-label"><b>Имя</b></label>
                            <div class="col-sm-9">
                                <font id="error"><?= form_error('name'); ?></font>
                                <input class="form-control" id="Name" type="text" placeholder="" name="name" value="<?php echo set_value('name');?>" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="contact-email" class="col-sm-3 control-label"><b>E-mail</b></label>
                            <div class="col-sm-9">
                                <font id="error"><?= form_error('email'); ?></font>
                                <input class="form-control" id="contact-email" type="text" placeholder="" name="email" value="<?php set_value('email');?>"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="contact-message" class="col-sm-3 control-label"><b>Message</b></label>
                            <div class="col-sm-9">
                                <font id="error"><?= form_error('message'); ?></font>
                                <textarea name="message" class="form-control" rows="5" id="contact-message"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" class="btn pull-right">Отправить</button>
                            </div>
                        </div>
                        <?php if(isset($sussed)): ?>
                            <div id="sussed"><?= $sussed;?></div><br />
                            <a href="/">&lsaquo;&lsaquo;Домой</a>
                        <?php endif;?>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>