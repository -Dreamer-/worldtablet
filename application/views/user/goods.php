<div class="eshop-section section">
    <div class="container goods">
        <div class="row">
            <form action="" method="get" style="display: inline; float: right; position: relative; top: -18px; left: -15px">
                <div class="input-group">
                    <input name="search" class="form-control" type="text" placeholder="Поиск..." style="display: block; float: right; width: 280px" />
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit"><span style="display: inline" class="glyphicon glyphicon-search"></span></button>
                        </span>
                </div>
            </form>
        </div>
        <div class="row">
            <?php if(!empty($output['form_error'])): ?>
            <div class="panel panel-danger">
                <div class="panel-heading">
                    Упс...
                </div>
                <div class="panel-body" style="text-align: center;"><?= $output['form_error']; ?></div>
            </div>
            <?php elseif(!empty($list_goods) OR !empty($output['list_goods'])): ?>
                <?php $goods = (!empty($output['list_goods'])) ? $output['list_goods'] : $list_goods; ?>
                <?php foreach($goods as $j): ?>
                    <div class="col-md-3 col-sm-6">
                        <!-- Product -->
                        <div class="shop-item" style="text-align: center">
                            <div class="shop-item-image image">
                                <a href="/product/<?= $j->goods_id; ?>"><img src="/upload/timthumb.php?src=/goods/<?= $j->image;?>&h=180"></a> <!-- &w=247 -->
                            </div>
                            <!-- Product Title -->
                            <div class="title" style="height: 58px">
                                <h3><a href="/product/<?= $j->goods_id; ?>">&nbsp;<?= $j->goods_title; ?></a></h3>
                            </div>
                            <!-- Product Price-->
                            <div class="price">
                                    $<?= $j->price; ?>
                            </div>
                            <!-- Add to Cart Button -->
                            <div class="actions">
                                <a href="/product/<?= $j->goods_id; ?>" class="btn btn-small"><i class="icon-shopping-cart icon-white"></i> Подробнее...</a>
                            </div>
                        </div>
                        <!-- End Product -->
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        Упс...
                    </div>
                    <div class="panel-body" style="text-align: center;">Товаров <?php if(!empty($output['request'])) {echo "по запросу \"{$output['request']}\"";}?> нет</div>
                </div>
            <?php endif; ?>
            <?php if(!empty($pagination)): ?>
                </div>
                <div class="pagination-wrapper ">
                    <ul class="pagination pagination-lg">
                        <?= $pagination; ?>
                    </ul>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>