<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title><?php echo $site_title, ' - ', $site_name ?></title>
    <meta name="description" content="<?php if(!empty($meta_desc)){echo $meta_desc;}?>" />
    <meta name="keywords" content="<?php if(!empty($meta_key)){echo $meta_key;}?>" />
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/icomoon-social.css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,600,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="/css/leaflet.css" />
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/css/leaflet.ie.css" />
    <![endif]-->
    <link rel="stylesheet" href="/css/main.css">

    <script src="/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>


    <link rel="stylesheet" href="/css/development.css" type="text/css" />
    <link rel="shortcut icon" href="/css/images/WT.ico" type="image/x-icon" />
</head>
<body>

<!--[if lt IE 7]>
<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
<![endif]-->

<div class="mainmenu-wrapper">
    <div class="container">
        <div class="menuextras">
            <div class="extras">
                <ul>
                    <li class="shopping-cart-items">
                        <i class="glyphicon glyphicon-shopping-cart icon-white"></i>
                        <a href="/cart"><b id="cart_total"><?= $this->cart->total_items();?> items</b></a>
                        <b id="lang"><?php if ($lang == 'ru') { echo '<a href="/lang/en">en</a>';} else { echo 'en';}?>
                            | <?php if ($lang == 'en') { echo '<a href="/lang/ru">ru</a>';} else { echo 'ru';}?></b> <!--TODO Действующий язык сделать неактивным -->
                    </li>
                </ul>
            </div>
        </div>
        <nav id="mainmenu" class="mainmenu">
            <ul>
                <li class="logo-wrapper"><a href="/" style="font-family: 'Lobster', cursive; font-size: 35px"><?= $site_name; ?></a></li>
                <?php if(!empty($get_menu)): ?>
                    <?php $for = 0; ?>
                    <?php foreach($get_menu as $key => $j): ?>
                        <li><a href="/page/<?php echo $j->link;?>"><span><?php echo $j->title; ?></span></a></li>
                        <?php $for++; ?>
                        <?php if($for == 1): ?>
                            <li><a href="/goods"><span id="show_goods"><?= ($lang == 'ru') ? 'Товары' : 'Goods'; ?></span></a></li>
                        <?php endif; ?>
                    <?php endforeach ?>
                    <li><a href="/feedback"><span id="show_goods"><?= ($lang == 'ru') ? 'Контакты' : 'Contacts'; ?></span></a></li>
                <?php else: echo 'id not fount'; ?>
                <?php endif; ?>
            </ul>
        </nav>
    </div>
</div>
<?php if(!empty($site_title) and $site_title != 'Главная' and $site_title != 'Home'): ?>
    <div class="section section-breadcrumbs">
        <div class="container">
            <div class="row goods">
                <div class="col-md-12">
                    <h1><?= $site_title ?></h1>
                </div>
                <?php if(!empty($categories)): ?>
                    <p class="category" style="font-size: 14px; display: inline-block; color: #ffffff; margin-top: 7px">
                        <?= $about.'&nbsp;<a href="/goods" id="link">Все</a>&nbsp;&nbsp;'; foreach($categories as $j){echo "<a id=\"link\" href='/goods/$j->categ_id'>$j->title</a>&nbsp;&nbsp;";}?>
                    </p>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endif; ?>