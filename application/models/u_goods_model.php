<?php
/**
 * Created by PhpStorm.
 * User: dreamer
 * Date: 10.07.15
 * Time: 15:46
 */

class U_Goods_model extends CI_Model {
    public function get_item($id) {
        return $this->db->get('goods', array('goods_id' => $id))->row();
    }

    public function for_pagination($limit, $page) {
        return $this->db->limit($limit, $page)->order_by('date_of_creation', 'desc')->get('goods')->result();
    }

    public function count() {
        return $this->db->count_all('goods');
    }

    public function search($text) {
        return $this->db->or_like(array('goods_title' => $text, 'text' => $text))->get('goods')->result();
    }

    public function commentaries($id) {
        return $this->db->order_by('date', 'DESC')->get_where('comments', array('product_id' => $id))->result();
    }

    public function count_commentaries($id) {
        return $this->db->where(array('product_id' => $id))->count_all_results('comments');
    }

    public function item_by_goods_id($id){
        $goods = $this->db->select('c.*, g.*, s.*')->join('categories c', 'c.categ_id=g.categ_id')->join('product_specification s', 's.id_goods=g.goods_id')->get_where('goods g', array('goods_id' => $id))->row();
        return $goods;
    }

    public function all_by_categ_id($id_category) {
        return $this->db->select('c.*, g.*')->join('categories c', 'c.categ_id=g.categ_id')->get_where('goods g', array('c.categ_id' => $id_category))->result();
    }
}