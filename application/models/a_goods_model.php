<?php
/**
 * Created by PhpStorm.
 * User: dreamer
 * Date: 07.07.15
 * Time: 15:52
 */

class A_Goods_model extends CI_Model {
    public function goods($sort = 0) {
        switch($sort){
            case 1 : $column = 'goods_title';   break;
            case 2 : $column = 'c.categ_id';    break;
            case 3 : $column = 'title';         break;
            case 4 : $column = 'price';         break;
            default : $column = 'goods_id';     break;
        }

        return $this->db->select('c.*, g.*')->join('categories c', 'c.categ_id=g.categ_id')->order_by($column)->get('goods g')->result();
    }

    public function content($id){
        return $this->db->select('s.*, g.*')->join('product_specification s', 's.id_goods=g.goods_id')->get_where('goods g', array('g.goods_id' => $id))->row();
    }

    public function create($goods, $specification) {
        $this->db->insert('goods', $goods);
        $id = $this->db->select('goods_id')->order_by('goods_id', 'desc')->limit('1')->get('goods')->row()->goods_id;
        $specification['id_goods'] = $id;
        $this->db->insert('product_specification', $specification);
    }

    public function edit($id, $goods, $specification) {
        $this->db->update('goods', $goods, "goods_id = $id");
        $this->db->update('product_specification', $specification, "id_goods = $id");
    }

    public function delete($id) {
        $file = $this->db->get_where('goods', array('goods_id' => $id))->row();
        $path = './upload/goods';
        unlink($path.DIRECTORY_SEPARATOR.$file->image);
        $this->db->delete('goods', array('goods_id' => $id));
        $this->db->delete('comments', array('product_id' => $id));
        $this->db->delete('product_specification', array('id_goods' => $id));
    }

    public function get_comment($id) {
        return $this->db->get_where('comments', array('product_id' => $id))->result();
    }

    public function delete_comment($id) {
        $this->db->delete('comments', array('id' => $id));
    }
}