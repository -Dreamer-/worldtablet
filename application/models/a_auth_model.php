<?php
/**
 * Created by PhpStorm.
 * User: dreamer
 * Date: 11.03.15
 * Time: 21:09
 */

class A_Auth_model extends CI_Model {
    public function admin($user, $pass) {
        return $this->db->get_where('admin', array('username' => $user, 'password' => $pass))->row();
    }

    public function add($attempts, $captcha = null, $ip_address = null) {
        $this->db->update('captcha', array('value' => $attempts), array('name' => 'login_attempts'));
        $this->db->update('captcha', array('value' => $captcha), array('name' => 'captcha'));
        if (!empty($ip_address)) $this->db->update('captcha', array('value' => $ip_address), array('name' => 'ip_address'));
    }

    public function getAttempts() {
        return $this->db->get_where('captcha', array('name' => 'login_attempts'))->row();
    }

    public function getCaptcha() {
        return $this->db->get_where('captcha', array('name' => 'captcha'))->row();
    }
}