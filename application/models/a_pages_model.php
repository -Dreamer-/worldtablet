<?php
/**
 * Created by PhpStorm.
 * User: dreamer
 * Date: 02.07.15
 * Time: 0:24
 */

class A_Pages_model extends CI_Model {
    public function get_item($id) {
        return $this->db->get_where('pages', array('id' => $id))->row();
    }

    public function create($data) {
        $this->db->insert('pages', $data);
    }

    public function edit($id, $data) {
        $this->db->update('pages', $data, array('id' => $id));
    }

    public function delete($id) {
        $this->db->delete('pages', array('id' => $id));
    }
}
