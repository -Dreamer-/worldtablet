<?php
/**
 * Created by PhpStorm.
 * User: dreamer
 * Date: 07.07.15
 * Time: 15:32
 */

class A_Categories_model extends CI_Model {
    public function get() {
        return $this->db->get('categories')->result();
    }

    public function get_item($id) {
        return $this->db->get_where('categories', array('categ_id' => $id))->row();
    }

    public function add($title) {
        $this->db->insert('categories', array('title' => $title));
    }

    public function edit($id, $data) {
        $this->db->update('categories', $data, array('categ_id' => $id));
    }

    public function delete($id) {
        $this->db->delete('categories', array('categ_id' => $id));
    }
}