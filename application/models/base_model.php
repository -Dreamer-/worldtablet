<?php
/**
 * Created by PhpStorm.
 * User: dreamer
 * Date: 12.02.15
 * Time: 15:35
 */

class Base_model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function get_content($link = '', $lang = 'ru') {
        return $this->db->get_where("pages_$lang", array('link' => $link))->row();
    }

    public function get_menu($lang = 'en') {
        return $this->db->select('id, title, link')->get("pages_$lang")->result();
    }

    public function feedback($name, $email, $message) {
        $query = array (
            'name' => $name,
            'email' => $email,
            'message' => $message
        );

        $this->db->insert('feedback', $query);
    }

    public function slide() {
        return $this->db->get_where('goods', array('best' => 1))->result();
    }

    public function basket_name($link) {
        return $this->db->get_where('goods', array('goods_id' => $link))->row();
    }

    public function add_customer($data = array()) {
        $this->db->insert('customer', $data);
    }

    public function add_order($data = array()) {
        $this->db->insert('orders', $data);
    }

    public function id() {
        return $this->db->insert_id();
    }

    public function recording_comment($name, $comment, $product_id, $date) {
        $this->db->insert('comments', array('name' => $name, 'comment' => $comment, 'product_id' => $product_id, 'date' => $date));
    }

    public function recommended($id) {
        return $this->db->select('c.*, g.*')->join('categories c', 'c.categ_id=g.categ_id')->limit(10)->get_where('goods g', array('c.categ_id' => $id))->result();
    }

    public function new_in_site() {
        $phone['phone'] = $this->db->limit(4)->order_by('date_of_creation', 'desc')->get_where('goods', array('categ_id' => 1))->result();
        $tablet['tablet'] = $this->db->limit(4)->order_by('date_of_creation', 'desc')->get_where('goods', array('categ_id' => 2))->result();
        return array_merge($phone, $tablet);
    }
}