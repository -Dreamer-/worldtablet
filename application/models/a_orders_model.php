<?php
/**
 * Created by PhpStorm.
 * User: dreamer
 * Date: 09.07.15
 * Time: 14:49
 */

class A_Orders_model extends CI_Model {
    public function get() {
        return $this->db->get('customer')->result();
    }

    public function item_customer($id) {
        return $this->db->get_where('customer', array('id_order' => $id))->row();
    }

    public function item_orders($id) {
        return $this->db->get_where('orders', array('order_id' => $id))->result();
    }

    public function update_status($id, $status) {
        return $this->db->update('customer', array('status' => $status), array('id_order' => $id));
    }

    public function delete($id) {
        $this->db->delete('orders', array('order_id' => $id));
        $this->db->delete('customer', array('id_order' => $id));
    }
}