<?php
/**
 * Created by PhpStorm.
 * User: dreamer
 * Date: 09.03.15
 * Time: 19:33
 */


require_once 'my_controller.php';
class Goods extends My_controller {
    public function __construct() {
        parent::__construct();
        $this->load->library('email');
        $this->load->helper('url');
        $this->load->model('a_categories_model');

        $email['newline'] = '\r\n';
        $email['wrapchars'] = '10000';
        $email['mailtype'] = 'html';
        $email['wordwrap'] = TRUE;
    }

    public function index($id = 0, $page = 0) {
        $this->load->library('pagination');
        $config = array(
            'base_url' => base_url().'goods/page/',
            'total_rows' =>  $this->u_goods_model->count(),
            'first_link' => 'В начало',
            'last_link' => 'В конец',
            'next_link' => 'Вперед',
            'prev_link' => 'Назад',
            'num_links' => 10,
            'per_page' => 20,

            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>',

            'prev_tag_open' => '<li>',
            'prev_tag_close' => '</li>',

            'next_tag_open' => '<li>',
            'next_tag_close' => '</li>',

            'first_tag_open' => '<li>',
            'first_tag_close' => '</li>',

            'last_tag_open' => '<li>',
            'last_tag_close' => '</li>',

            'cur_tag_open' => '<li class="active"><a>',
            'cur_tag_close' => '</a></li>',
        );
        $this->pagination->initialize($config);

        $data['categories'] = $this->a_categories_model->get();
        $data['about'] = 'Категории: ';


        if (!empty($this->input->get('search'))) {
            $request = $this->input->get('search', TRUE);
            $data['output'] = $this->mini_search($request);
        } else {
            if ($id != 0) {
                $data['list_goods'] = $this->u_goods_model->all_by_categ_id($id);
                $categ              = $this->a_categories_model->get_item($id);
                $data['meta_desc']  = "Все товары в категории '$categ->title'";
                $data['meta_key']   = "Телефоны, планшеты, $categ->title";
            } else {
                $data['list_goods'] = $this->u_goods_model->for_pagination($config['per_page'], $page);
                $data['meta_desc']  = "Продажа техники, не дорого";
                $data['meta_key']   = "Телефоны, планшеты, продажа кошек";
                $data['pagination'] = $this->pagination->create_links();
            }
        }


        $this->set_title('Товары');
        $this->template('goods', $data);
    }

    public function mini_search($request, $data = NULL) {
        if (!empty($request) AND strlen($request) < 3) {
            $data['form_error'] = "Запрос должен содержать не менее 3-х символов";
        } elseif(!empty($request)) {
            $data['list_goods'] = $this->u_goods_model->search($request);
            $data['request'] = "$request";
        }
        return $data;
    }

    public function search() {
            $data = NULL;
            $this->set_title('Поиск');
            $this->template('search', $data);
    }
}