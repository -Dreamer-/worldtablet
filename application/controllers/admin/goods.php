<?php
/**
 * Created by PhpStorm.
 * User: dreamer
 * Date: 13.03.15
 * Time: 12:37
 */

require_once 'admin_controller.php';
class Goods extends Admin_controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library(array('form_validation', 'upload'));

        $load = array(
            'upload_path' => './upload/goods/',
            'allowed_types' => 'gif|jpg|png',
            'max_size' => '3028',
            'max_filename' => '25',
            'encrypt_name' => TRUE
        );
        $this->upload->initialize($load);

        $session_id = $this->session->userdata('id_user');
        if(empty($session_id)) {redirect('/admin');}
    }

    public function index($sort = 0) {
        $data['username'] = $this->session->userdata('username');
        $data['goods'] = $this->a_goods_model->goods($sort);

        $this->set_title('goods');
        $this->template('admin/goods', $data);
    }

    protected function my_set_rules() {
        $this->form_validation->set_rules('title',          'Goods title',          'trim|required|max_length[60]|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('text',           'Goods text',           'trim|required|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('category',       'Goods category',       'trim|required|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('price',          'Goods price',          'trim|required|numeric|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('best',           'best',                 'trim|numeric|xxs_clean|prep_for_form');


        $this->form_validation->set_rules('manufacturer',   'Goods manufacturer',   'trim|required|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('model',          'Goods model',          'trim|required|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('display',        'Goods display',        'trim|required|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('memory',         'Goods memory',         'trim|required|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('relations',      'Goods relations',      'trim|required|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('sizes',          'Goods sizes',          'trim|required|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('slots',          'Goods slots',          'trim|required|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('processor',      'Goods processor',      'trim|required|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('system',         'Goods system',         'trim|required|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('battery',        'Goods battery',        'trim|required|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('camera',         'Goods camera',         'trim|required|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('weight',         'Goods processor',      'trim|required|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('other',          'Goods other',          'trim|required|xxs_clean|prep_for_form');
    }

    public function add() {
        $data['username'] = $this->session->userdata('username');
        $data['categories'] = $this->a_categories_model->get();

        $this->my_set_rules();

        if ($this->form_validation->run() == TRUE) {
            $goods = array(
                'goods_title'       =>      $this->input->post('title', TRUE),
                'text'              =>      $this->input->post('text', TRUE),
                'categ_id'          =>      $this->input->post('category', TRUE),
                'price'             =>      $this->input->post('price', TRUE),
                'best'              =>      $this->input->post('best'),
                'meta_desc'         =>      $this->input->post('title', TRUE).', $'.$this->input->post('price', TRUE),
                'meta_key'          =>      $this->input->post('title', TRUE).', $'.$this->input->post('price', TRUE),
            );

            $specification = array(
                'manufacturer'      =>      $this->input->post('manufacturer', TRUE),
                'model'             =>      $this->input->post('model', TRUE),
                'display'           =>      $this->input->post('display', TRUE),
                'memory'            =>      $this->input->post('memory', TRUE),
                'relations'         =>      $this->input->post('relations', TRUE),
                'sizes'             =>      $this->input->post('sizes', TRUE),
                'slots'             =>      $this->input->post('slots', TRUE),
                'processor'         =>      $this->input->post('processor', TRUE),
                'system'            =>      $this->input->post('system', TRUE),
                'battery'           =>      $this->input->post('battery', TRUE),
                'camera'            =>      $this->input->post('camera', TRUE),
                'weight'            =>      $this->input->post('weight', TRUE),
                'other'             =>      $this->input->post('other', TRUE)
            );

            if($this->upload->do_upload()) {
                $img = $this->upload->data();
                $goods['image'] = $img['file_name'];
            } else {$warning = $this->upload->display_errors();}

            if(!empty($warning)) {
                $data['warning'] = $warning;
            } else {
                $this->a_goods_model->create($goods, $specification);
                redirect('/admin/goods');
            }
        }

        $this->set_title('new_goods');
        $this->template('admin/new_goods', $data);
    }

    public function edit($id) {
        $data['username'] = $this->session->userdata('username');
        $data['content'] = $this->a_goods_model->content($id);
        $data['categories'] = $this->a_categories_model->get();

        $this->my_set_rules();

        if ($data['content']->best == 1) {
            $data['checked'] = 'checked="checked"';
        }

        if ($this->form_validation->run() == TRUE) {
            $goods = array(
                'goods_title'       =>      $this->input->post('title', TRUE),
                'text'              =>      $this->input->post('text', TRUE),
                'categ_id'          =>      $this->input->post('category', TRUE),
                'price'             =>      $this->input->post('price', TRUE),
                'best'              =>      $this->input->post('best'),
                'meta_desc'         =>      $this->input->post('title', TRUE).', $'.$this->input->post('price', TRUE),
                'meta_key'          =>      $this->input->post('title', TRUE).', $'.$this->input->post('price', TRUE),
            );

            $specification = array(
                'manufacturer'      =>      $this->input->post('manufacturer', TRUE),
                'model'             =>      $this->input->post('model', TRUE),
                'display'           =>      $this->input->post('display', TRUE),
                'memory'            =>      $this->input->post('memory', TRUE),
                'relations'         =>      $this->input->post('relations', TRUE),
                'sizes'             =>      $this->input->post('sizes', TRUE),
                'slots'             =>      $this->input->post('slots', TRUE),
                'processor'         =>      $this->input->post('processor', TRUE),
                'system'            =>      $this->input->post('system', TRUE),
                'battery'           =>      $this->input->post('battery', TRUE),
                'camera'            =>      $this->input->post('camera', TRUE),
                'weight'            =>      $this->input->post('weight', TRUE),
                'other'             =>      $this->input->post('other', TRUE)
            );

            if($this->upload->do_upload()) {
                $path = rtrim('./upload/goods');
                unlink($path.DIRECTORY_SEPARATOR.$data['content']->image);

                $img = $this->upload->data();
                $goods['image'] = $img['file_name'];
            } else {$goods['image'] = $data['content']->image;}

            if(!empty($warning)) {
                $data['warning'] = $warning;
            } else {
                $this->a_goods_model->edit($id, $goods, $specification);
                redirect('/admin/goods');
            }
        }

        $data['get_comment'] = $this->a_goods_model->get_comment($id);

        $this->set_title('goods_edit');
        $this->template('admin/goods_edit', $data);
    }

    public function delete_comment($p_id, $id) {
        $this->a_goods_model->delete_comment($id);
        redirect("/admin/goods_edit/$p_id");
    }

    public function delete($id) {
        $this->a_goods_model->delete($id);
        redirect('/admin/goods/');
    }
}