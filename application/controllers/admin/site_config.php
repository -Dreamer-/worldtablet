<?php
/**
 * Created by PhpStorm.
 * User: dreamer
 * Date: 27.03.15
 * Time: 20:48
 */

require_once 'admin_controller.php';
class Site_config extends Admin_controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->model('base_model');
        $this->load->library(array('form_validation', 'upload'));

        $load_logo = array(
            'upload_path' => './img/logo/',
            'allowed_types' => 'gif|jpg|png',
            'file_name' => 'logo',
            'max_size' => '1028',
            'max_filename' => '25',
            'encrypt_name' => FALSE
        );
        $this->upload->initialize($load_logo);

        $session_id = $this->session->userdata('id_user');
        if(empty($session_id)) {redirect('/admin');}
    }

    public function index() {
        $data['username'] = $this->session->userdata('username');
        $data['site_name'] = $this->a_site_config_model->get('site_name');
        $data['site_logo'] = $this->a_site_config_model->get('site_logo');

        $this->form_validation->set_rules('name', 'Page title', 'trim|required|max_length[50]|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('phone', 'Page title', 'trim|max_length[20]|xxs_clean|prep_for_form');

        if($this->form_validation->run() == TRUE) {
            $config['name'] = $this->input->post('name');
            //$phone = $_POST['phone'];

            if($this->upload->do_upload()) {
                $path = rtrim('./img/logo');
                unlink($path.DIRECTORY_SEPARATOR.$data['site_logo']->value);

                $img = $this->upload->data();
                $config['image'] = $img['file_name'];
            } else {
                $config['image'] = $data['site_logo']->value;
                $data['warning'] = $this->upload->display_errors();
            }

            $this->a_site_config_model->update($config);

            redirect('/admin/config');
        }

        $this->set_title('config');
        $this->template('admin/site_config', $data);
    }
}
