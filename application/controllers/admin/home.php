<?php
/**
 * Created by PhpStorm.
 * User: dreamer
 * Date: 11.02.15
 * Time: 11:59
 */

require_once 'admin_controller.php';
class Home extends Admin_controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');

        $session_id = $this->session->userdata('id_user');
        if(empty($session_id)) {redirect('/admin');}
    }

    public function index() {
        $data['username'] = $this->session->userdata('username');

        $this->set_title('home');

        $this->template('admin/home', $data);

    }
}