<?php
/**
 * Created by PhpStorm.
 * User: dreamer
 * Date: 05.03.15
 * Time: 13:03
 */

require_once 'admin_controller.php';
class Messages extends Admin_controller {
    public function __construct() {
        parent::__construct();

        $session_id = $this->session->userdata('id_user');
        if(empty($session_id)) {redirect('/admin');}
    }

    public function index() {
        $data['username'] = $this->session->userdata('username');
        $data['messages'] = $this->a_messages_model->messages();

        $this->set_title('messages');
        $this->template('admin/messages', $data);
    }

    public function edit_status($id) {
        $this->a_messages_model->edit($id);
        redirect('/admin/messages');
    }

    public function delete($id) {
        $this->a_messages_model->delete($id);
        redirect('/admin/messages');
    }
}
