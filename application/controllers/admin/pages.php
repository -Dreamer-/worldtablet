<?php
/**
 * Created by PhpStorm.
 * User: dreamer
 * Date: 02.03.15
 * Time: 19:31
 */

require_once 'admin_controller.php';
class Pages extends Admin_controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');

        $session_id = $this->session->userdata('id_user');
        if(empty($session_id)) {redirect('/admin');}
    }

    public function index() {
        $data['username'] = $this->session->userdata('username');
        $this->set_title('pages');
        $this->template('admin/pages', $data);
    }

    protected function form($id = 0) {
        $this->form_validation->set_rules('title', 'Page title', 'trim|required|max_length[20]|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('link', 'Page link', 'trim|required|max_length[20]|alpha_dash|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('text', 'Page content', 'trim|required|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('description', 'Page description', 'trim|required|min_length[5]|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('keys', 'Page key', 'trim|required|min_length[5]|xxs_clean|prep_for_form');

        if ($this->form_validation->run() == TRUE) {
            $array = array(
                'title' => $this->input->post('title', TRUE),
                'link' => $this->input->post('link', TRUE),
                'text' => $this->input->post('text', TRUE),
                'meta_desc' => $this->input->post('description', TRUE),
                'meta_key' => $this->input->post('keys', TRUE)
            );
            if($id !== 0) {
                $this->a_pages_model->edit($id, $array);
            } else {
                $this->a_pages_model->create($array);
            }
            redirect('/admin/pages');
        }
    }

    public function new_page() {
        $data['username'] = $this->session->userdata('username');

        $this->form();

        $this->set_title('new_page');
        $this->template('admin/new_page', $data);
    }

    public function page_edit($id) {
        $data['username'] = $this->session->userdata('username');
        $data['content'] = $this->a_pages_model->get_item($id);

        $this->form($id);

        $this->set_title('edit');
        $this->template('admin/page_edit', $data);
    }

    public function delete($id) {
        $this->a_pages_model->delete($id);
        redirect('/admin/pages');
    }
}
