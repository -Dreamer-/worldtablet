<?php
/**
 * Created by PhpStorm.
 * User: dreamer
 * Date: 18.02.15
 * Time: 22:25
 */

require_once 'admin_controller.php';
class Auth extends Admin_controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper(array('captcha', 'url'));
    }

    const ATTEMPTS_ZERO = 0;

    public function index() {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('username', 'username', 'trim|required|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('password', 'password', 'trim|required|xxs_clean|prep_for_form');
        $data = array();

        $attempts = $this->a_auth_model->getAttempts()->value;
        if (!empty($captcha = $this->loginCaptcha($attempts))) {
            $data['captcha'] = $captcha['image'];
        }
        $db_captcha = $this->a_auth_model->getCaptcha()->value;

        if ($this->form_validation->run() == TRUE) {
            $user = $this->input->post('username', TRUE);
            $pass = $this->input->post('password', TRUE);
            $capt = $this->input->post('captcha', TRUE);

            $query = $this->a_auth_model->admin($user, $pass);
            if (!empty($query) and empty($db_captcha) or $db_captcha == $capt and !empty($query)) {
                $this->session->set_userdata(array('id_user' => $query->id, 'username' => $user));
                $this->a_auth_model->add(self::ATTEMPTS_ZERO);
                redirect('/admin/home');
            } else {
                $data['error_message'] = 'The data entered is incorrect, please try again.';

                $ip = $this->input->ip_address();
                $attempts++;
                $this->a_auth_model->add($attempts, $captcha['word'], $ip);
            }
        }

        $session_id = $this->session->userdata('id_user');
        if(!empty($session_id)) {redirect('/admin/home');}


        $this->set_title('Админка');
        $data = array_merge($this->settings, $data);
        $this->load->view('admin/auth', $data);
    }

    protected function loginCaptcha($attempts) {
        $setting = array(
            'img_path'	=> './img/captcha/',
            'img_url'	=>  base_url().'img/captcha/'
        );

        $captcha = null;
        if($attempts >= 2) {
            $captcha = create_captcha($setting);
        }
        return $captcha = array(
            'image' => $captcha['image'],
            'word' => $captcha['word'],
            'attempts' => $attempts
        );
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('/admin');
    }
}