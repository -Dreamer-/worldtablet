<?php
/**
 * Created by PhpStorm.
 * User: dreamer
 * Date: 14.03.15
 * Time: 12:53
 */

require_once 'admin_controller.php';
class Accounts extends Admin_controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library(array('form_validation'));

        $session_id = $this->session->userdata('id_user');
        $session_username = $this->session->userdata('username');
        if(empty($session_id)) {redirect('/admin');}
        elseif($session_id != 1 AND $session_username !== 'admin') {redirect('/admin/home');}
    }

    public function form_run($id = 0) {
        $this->form_validation->set_rules('username', 'Username' ,'trim|required|max_length[30]|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('password', 'Password' ,'trim|required|xxs_clean|prep_for_form');
        $this->form_validation->set_rules('repeat', 'Repeat password' ,'trim|required|xxs_clean|prep_for_form');

        $post = array(
            'id' => $id,
            'username' => $this->input->post('username', TRUE),
            'password' => $this->input->post('password', TRUE),
            'repeat' => $this->input->post('repeat', TRUE)
        );

        if($this->form_validation->run() == TRUE) {
            if($post['password'] === $post['repeat']) {
                switch ($id){
                    case !0 : $this->a_accounts_model->edit($post);
                        break;
                    case 0 : $this->a_accounts_model->add($post);
                        break;
                }
                redirect('/admin/users');
            }
        }
    }

    public function index() {
        $data['username'] = $this->session->userdata('username');
        $data['user'] = $this->a_accounts_model->get();

        $this->form_run();
        $data['error_pass'] = ($this->input->post('password') !== $this->input->post('repeat')) ? 'You not correctly repeated password' : NULL;

        $this->set_title('accounts');
        $this->template('admin/accounts', $data);
    }

    public function create() {
        $data['username'] = $this->session->userdata('username');

        $this->form_run();
        $data['error_pass'] = ($this->input->post('password') !== $this->input->post('repeat')) ? 'You not correctly repeated password' : NULL;

        $this->set_title('create_accounts');
        $this->template('admin/create_accounts', $data);
    }

    public function edit($id) {
        $data['username'] = $this->session->userdata('username');
        $data['user'] = $this->a_accounts_model->get_item($id);

        $this->form_run($id);
        $data['error_pass'] = ($this->input->post('password') !== $this->input->post('repeat')) ? 'You not correctly repeated password' : NULL;

        $this->set_title('account_edit');
        $this->template('admin/account_edit', $data);
    }

    public function delete($id) {
        $this->a_accounts_model->delete($id);
        redirect('/admin/users');
    }
}