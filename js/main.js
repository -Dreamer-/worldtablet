$(function () {
    $('#check_out').click(function () {
        $('#check_out_form').css('display', 'block');
    });

    $('#open_specification').click(function () {
        $('#open_specification').attr('id', 'close_specification').attr('onclick', 'closeSpecification()').html('- Specification');
        $('#specification').css('display', 'block');
        return false;
    });

    /*$('.sendComment').click(function() {
        $('.commentaries').animate({
            scrollTop: 0
        },
        {
            duration: 500,
            easing: "swing"
        })
    })*/
});

function sendToCart() {
    var data = $("#send_to_cart").serialize();
    $.ajax({
        type: 'POST',
        url: '/products/processing_form',
        data: data,
        success: function() {
            alert('Товар добавлен в корзину');
            $.post('/cart_items', function(data){
                $('#cart_total').html(data)
            })
        }
    });
    return false;
}

function cleanBasket() {
    $.ajax({
        url: '/cart/delete_all',
        success: function(data) {
            $('.basket-cart').html(data)
        }
    });
    return false;
}

function addComment(id) {
    var data_comment = $(".addComment").serialize();
    $.ajax({
        type: 'POST',
        url: '/add_comment/'+id,
        data: data_comment,
        success: function(data) {
            $('.post-coments').html(data)
        }
    });
    return false;
}

function closeSpecification() {
    $('#close_specification').attr('id', 'open_specification').html('+ Specification');
    $('#specification').css('display', 'none');
    return false;
}
